#!/bin/bash

echo "Updating system ..."
yum -y update

echo "Installing development tools ..."
yum -y install nodejs npm
node --version
npm --version

echo "Installing hubot ..."
yum -y install libevent-devel
npm install -g hubot coffee-script
hubot --create hubot

